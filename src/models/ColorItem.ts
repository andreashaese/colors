export interface ColorItem {
    id: number
    name: string
    hex: string
}
