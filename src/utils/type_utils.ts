export const isArray = (input: unknown): input is any[] => Array.isArray(input)

export const isRecord = (input: unknown): input is Record<string, unknown> =>
    typeof input === 'object' && !isArray(input) && input !== null

export const isNumber = (input: unknown): input is number =>
    typeof input === 'number'

export const isString = (input: unknown): input is string =>
    typeof input === 'string'
