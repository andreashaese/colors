import { isArray, isNumber, isRecord, isString } from './type_utils'

describe('isArray', () => {
    test('is true for array', () => {
        expect(isArray([])).toBeTruthy()
    })

    test.each<unknown>([
        0,
        false,
        null,
        undefined,
        'some string',
        { message: 'this is an object' },
        () => 'some function',
    ])('is false for non-arrays', (value) => {
        expect(isArray(value)).toBeFalsy()
    })
})

describe('isRecord', () => {
    test('is true for object', () => {
        expect(isRecord({})).toBeTruthy()
    })

    test.each<unknown>([
        0,
        false,
        null,
        undefined,
        'some string',
        [1, 2, 3],
        () => 'some function',
    ])('is false for non-objects', (value) => {
        expect(isRecord(value)).toBeFalsy()
    })
})

describe('isNumber', () => {
    test('is true for a number', () => {
        expect(isNumber(42)).toBeTruthy()
    })

    test.each<unknown>([
        '0',
        false,
        null,
        undefined,
        'some string',
        { message: 'this is an object' },
        [1, 2, 3],
        () => 'some function',
    ])('is false for non-numbers', (value) => {
        expect(isNumber(value)).toBeFalsy()
    })
})

describe('isString', () => {
    test('is true for a string', () => {
        expect(isString('Hello world')).toBeTruthy()
    })

    test.each<unknown>([
        0,
        false,
        null,
        undefined,
        { message: 'this is an object' },
        [1, 2, 3],
        () => 'some function',
    ])('is false for non-strings', (value) => {
        expect(isString(value)).toBeFalsy()
    })
})
