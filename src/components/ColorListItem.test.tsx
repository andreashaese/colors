import { render, screen } from '@testing-library/react'
import ColorListItem from './ColorListItem'

test('shows the color name and the hex code', () => {
    render(<ColorListItem name="red" hex="#ff0000" />)

    expect(screen.getByText('red (#ff0000)')).toBeInTheDocument()
})
