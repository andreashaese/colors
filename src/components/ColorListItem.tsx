import React, { ReactElement } from 'react'

interface Props {
    name: string
    hex: string
}

const ColorListItem = ({ name, hex }: Props): ReactElement => (
    <li style={{ color: hex }}>{`${name} (${hex})`}</li>
)

export default ColorListItem
