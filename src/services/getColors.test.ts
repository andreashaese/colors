import getColors, { sampleApiUrl } from './getColors'
import { ColorItem } from '../models/ColorItem'
import { rest } from 'msw'
import { setupServer } from 'msw/node'

const server = setupServer()

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test.each([
    {},
    null,
    'some string',
    { id: 1, name: 'red', hex: '#ff0000' },
    ['one', 'two'],
])('returns an empty array if the input format is invalid', async (value) => {
    server.use(handlerWithResponse(value))

    const result = await getColors()

    await expect(result).toHaveLength(0)
})

test('returns the correct ColorItems', async () => {
    const data: ColorItem[] = [
        {
            id: 1,
            name: 'green',
            hex: '#00ff00',
        },
        {
            id: 2,
            name: 'blue',
            hex: '#0000ff',
        },
    ]
    server.use(handlerWithResponse(data))

    const result = await getColors()

    expect(result).toStrictEqual(data)
})

test('filters out parts that are not ColorItems', async () => {
    const data = [
        {
            id: 1,
            name: 'green',
            hex: '#00ff00',
        },
        {
            id: 'invalid',
        },
    ]
    server.use(handlerWithResponse(data))

    const result = await getColors()

    expect(result).toStrictEqual<ColorItem[]>([
        {
            id: 1,
            name: 'green',
            hex: '#00ff00',
        }
    ])
})

test('throws an error when response is not 200 OK', async () => {
    server.use(handlerWithStatusCode(400))

    await expect(() => getColors()).rejects
        .toThrowError('Bad status code: 400')
})

// Helpers

const handlerWithResponse = <T>(response: T) =>
    rest.get(
        sampleApiUrl,
        (req, res, ctx) => {
            return res(ctx.json(response))
        }
    )

const handlerWithStatusCode = (statusCode: number) =>
    rest.get(
        sampleApiUrl,
        (req, res, ctx) => {
            return res(ctx.status(statusCode))
        }
    )
