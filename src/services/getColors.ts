import { ColorItem } from '../models/ColorItem'
import { isArray, isNumber, isRecord, isString } from '../utils/type_utils'

const getColors = async (): Promise<ColorItem[]> => {
    const json = await fetchSampleApiData()
    return mapToColorItems(json)
}

export default getColors

export const sampleApiUrl = 'https://api.sampleapis.com/csscolornames/colors'

// Helpers

const fetchSampleApiData = async () => {
    const response = await fetch(sampleApiUrl)

    if (!response.ok) {
        throw new Error(`Bad status code: ${response.status}`)
    }

    return await response.json()
}

const isColorItem = (input: unknown): input is ColorItem =>
    isRecord(input)
    && isNumber(input.id)
    && isString(input.name)
    && isString(input.hex)

const mapToColorItems = (json: unknown): ColorItem[] => {
    if (!isArray(json)) {
        return []
    }

    return json.reduce<ColorItem[]>((items, currentItem) => {
        if (isColorItem(currentItem)) {
            items.push(currentItem)
        }

        return items
    }, [])
}

