import React, {
    ChangeEvent,
    FormEvent,
    useEffect,
    useMemo,
    useState,
} from 'react'
import getColors from '../services/getColors'
import ColorListItem from '../components/ColorListItem'
import { ColorItem } from '../models/ColorItem'

const ColorPage = (): React.ReactElement => {
    const {
        isLoading,
        colors,
        filter,
        error,
        reload,
        updateFilter,
        applyFilter,
        resetFilter,
    } = useColorPageState()

    const setFilter = (event: ChangeEvent<HTMLInputElement>): void => {
        event.preventDefault()
        updateFilter(event.target.value)
    }

    const confirmFilter = (event: FormEvent<HTMLFormElement>): void => {
        event.preventDefault()
        applyFilter()
    }

    if (isLoading) {
        return <h1>Loading colors...</h1>
    }

    if (error !== null) {
        return (
            <>
                <h1>Failed to fetch colors :(</h1>
                <p>{`An error occurred: ${error}`}</p>
                <button onClick={reload}>Try again</button>
            </>
        )
    }

    return (
        <>
            <form onSubmit={confirmFilter}>
                <label htmlFor="colorName">Search colors by name:</label>{' '}
                <input
                    type="text"
                    value={filter}
                    id="colorName"
                    placeholder="Color name"
                    aria-label="Color name"
                    onChange={setFilter}
                />{' '}
                <button>Filter</button>{' '}
                <button type="button" onClick={resetFilter}>
                    Reset
                </button>
            </form>
            <h1>Colors</h1>
            <ul>
                {colors.map((item) => (
                    <ColorListItem key={item.id} {...item} />
                ))}
            </ul>
        </>
    )
}

export default ColorPage

// Helpers

const useColorPageState = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState<string | null>(null)
    const [allColors, setAllColors] = useState<ColorItem[]>([])
    const [filter, setFilter] = useState('')
    const [appliedFilter, setAppliedFilter] = useState('')

    useEffect(() => {
        (async () => await reload())()
    }, [])

    const reload = async (): Promise<void> => {
        setIsLoading(true)
        setError(null)
        setAllColors([])

        try {
            const colors = await getColors()
            setAllColors(colors)
        } catch (e) {
            setError(e instanceof Error ? e.message : String(e))
        }

        setIsLoading(false)
    }

    const updateFilter = (newFilter: string): void => {
        setFilter(newFilter)
    }

    const applyFilter = (): void => {
        setAppliedFilter(filter)
    }

    const resetFilter = (): void => {
        setFilter('')
        setAppliedFilter('')
    }

    const colors = useMemo(() => {
        return allColors.filter(color => color.name.includes(appliedFilter))
    }, [allColors, appliedFilter])

    return {
        isLoading,
        error,
        colors,
        filter,
        reload,
        updateFilter,
        applyFilter,
        resetFilter,
    }
}
