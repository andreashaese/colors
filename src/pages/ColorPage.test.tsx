import React from 'react'
import {
    render,
    screen,
    waitForElementToBeRemoved,
} from '@testing-library/react'
import ColorPage from './ColorPage'
import getColors from '../services/getColors'
import { ColorItem } from '../models/ColorItem'
import userEvent from '@testing-library/user-event'

jest.mock('../services/getColors')

test('shows loading text while fetching', async () => {
    configureGetColorsToKeepLoading()

    render(<ColorPage />)

    expect(loadingText()).toBeInTheDocument()
})

test('shows error message when fetching fails', async () => {
    configureGetColorsToFail()

    render(<ColorPage />)
    await waitForElementToBeRemoved(loadingText)

    expect(errorText('Some failure')).toBeInTheDocument()
})

test('shows colors after failing first, then retrying', async () => {
    configureGetColorsToFail()
    render(<ColorPage />)
    await waitForElementToBeRemoved(loadingText)
    configureGetColorsToSucceed([
        {
            id: 1,
            name: 'red',
            hex: '#ff0000',
        },
        {
            id: 2,
            name: 'green',
            hex: '#00ff00',
        },
        {
            id: 3,
            name: 'bluegreen',
            hex: '#00ffff',
        },
    ])

    userEvent.click(tryAgainButton())
    await waitForElementToBeRemoved(loadingText)

    await showsColorItems([
        'red (#ff0000)',
        'green (#00ff00)',
        'bluegreen (#00ffff)',
    ])
})

test('shows all colors after loading', async () => {
    configureGetColorsToSucceed([
        {
            id: 1,
            name: 'red',
            hex: '#ff0000',
        },
        {
            id: 2,
            name: 'green',
            hex: '#00ff00',
        },
        {
            id: 3,
            name: 'bluegreen',
            hex: '#00ffff',
        },
    ])

    render(<ColorPage />)
    await waitForElementToBeRemoved(loadingText)

    showsColorItems([
        'red (#ff0000)',
        'green (#00ff00)',
        'bluegreen (#00ffff)',
    ])
})


describe('shows matching colors when name is entered', () => {
    test('and filter button is pressed', async () => {
        configureGetColorsToSucceed([
            {
                id: 1,
                name: 'red',
                hex: '#ff0000',
            },
            {
                id: 2,
                name: 'green',
                hex: '#00ff00',
            },
            {
                id: 3,
                name: 'bluegreen',
                hex: '#00ffff',
            },
        ])
        render(<ColorPage />)
        await waitForElementToBeRemoved(loadingText)

        enterFilterAndPressFilterButton('green')

        showsColorItems([
            'green (#00ff00)',
            'bluegreen (#00ffff)',
        ])
    })

    test('and enter key is pressed', async () => {
        configureGetColorsToSucceed([
            {
                id: 1,
                name: 'red',
                hex: '#ff0000',
            },
            {
                id: 2,
                name: 'green',
                hex: '#00ff00',
            },
            {
                id: 3,
                name: 'bluegreen',
                hex: '#00ffff',
            },
        ])
        render(<ColorPage />)
        await waitForElementToBeRemoved(loadingText)

        enterFilterAndPressEnter('green')

        showsColorItems([
            'green (#00ff00)',
            'bluegreen (#00ffff)',
        ])
    })
})

test('shows all colors again when filter is reset', async () => {
    configureGetColorsToSucceed([
        {
            id: 1,
            name: 'red',
            hex: '#ff0000',
        },
        {
            id: 2,
            name: 'green',
            hex: '#00ff00',
        },
        {
            id: 3,
            name: 'bluegreen',
            hex: '#00ffff',
        },
    ])
    render(<ColorPage />)
    await waitForElementToBeRemoved(loadingText)
    enterFilterAndPressFilterButton('green')

    resetFilter()

    showsColorItems([
        'red (#ff0000)',
        'green (#00ff00)',
        'bluegreen (#00ffff)',
    ])
})

// Configurations

const configureGetColorsToKeepLoading = (): void => {
    // Return a Promise that never resolves
    (getColors as jest.Mock).mockImplementation(() => new Promise(() => {}))
}

const configureGetColorsToFail = (): void => {
    (getColors as jest.Mock).mockRejectedValue(new Error('Some failure'))
}

const configureGetColorsToSucceed = (colorItems: ColorItem[]): void => {
    (getColors as jest.Mock).mockResolvedValue(colorItems)
}

// Element queries

const loadingText = () => screen.getByText('Loading colors...')

const errorText = (text: string) => screen.getByText(`An error occurred: ${text}`)

const tryAgainButton = () => screen.getByRole('button', { name: 'Try again' })

const filterTextField = () => screen.getByRole('textbox', { name: 'Color name' })

const filterButton = () => screen.getByRole('button', { name: 'Filter' })

const resetFilterButton = () => screen.getByRole('button', { name: 'Reset' })

const listItems = () => screen.getAllByRole('listitem')

// Interactions

const enterFilterAndPressFilterButton = (query: string): void => {
    userEvent.type(filterTextField(), query)
    userEvent.click(filterButton())
}

const enterFilterAndPressEnter = (query: string): void => {
    userEvent.type(filterTextField(), query)
    userEvent.type(filterTextField(), '{enter}')
}

const resetFilter = (): void => {
    userEvent.click(resetFilterButton())
}

// Assertions

const showsColorItems = (items: string[]): void => {
    const currentListItems = listItems()
    expect(currentListItems).toHaveLength(items.length)

    for (const [index, item] of items.entries()) {
        expect(currentListItems[index]).toHaveTextContent(item)
    }
}
